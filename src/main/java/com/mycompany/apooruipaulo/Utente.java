/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

//Classe Utente
/**
 * Representa um Utente
 * @author pcoelho
 */
public class Utente extends Pessoa {

    private char sexo; // sexo do utente
    private LocalDate dataNascimento; //Data de nascimento de utente
    private int nUtente; //Número de utente (9digitos)

    //contrutor
    /**
     * Cria um Utente com nome, nº telemovel, sexo, data de nascimeto e número
     * de utente
     *
     * @author pcoelho
     * @param nomePessoa nome de Utente 
     * @param nTelem nº de telemóvel de Utente 
     * @param sexo sexo de Utente
     * @param dataNascimento data de nascimento de Utente
     * @param nUtente nº de Utente
     */
    public Utente(String nomePessoa, int nTelem, char sexo, LocalDate dataNascimento, int nUtente) {
        super(nomePessoa, nTelem);
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.nUtente = nUtente;

    }

    /**
     * Cria um Utente sem atributos
     *
     * @author pcoelho
     */
    public Utente() {

    }

    // getter sexo de Utente
    /**
     * Getter sexo de Utente
     *
     * @author pcoelho
     * @return sexo sexo de Utente
     *
     */
    char getSexo() {
        return this.sexo;
    }

    // getter data de nascimento do utente
    /**
     * Getter data de Nascimento de Utente
     *
     * @author pcoelho
     * @return dataNascimento data de nascimento de Utente
     *
     */
    LocalDate getDataNascimento() {
        return this.dataNascimento;
    }

    // getter numero de utente
    /**
     * Getter número de utente
     *
     * @author pcoelho
     * @return nUtente nº de Utente
     *
     */
    int getnUtente() {
        return this.nUtente;
    }

    // setter sexo utente
    /**
     * Getter sexo de Utente
     *
     * @author pcoelho
     * @param sexo sexo de Utente
     *
     */
    void setsexo(char sexo) {
        this.sexo = sexo;
    }

    // setter data nascimento utente
    /**
     * Getter data de Nascimento de Utente
     *
     * @author pcoelho
     * @param dataNascimento data de nascimento de Utente
     */
    void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    // setter numero de utente
    /**
     * Setter número de utente
     *
     * @author pcoelho
     * @param nUtente nº de Utente
     */
    void setNumeroUtente(int nUtente) {
        this.nUtente = nUtente;
    }

    /**
     * Função/Metodo calcula a idade do utente em função de uma data fornecida
     * e a data de hoje
     *
     * @param data Objecto LocalDate
     * @return idade idade do utente
     */
    public static int idadeUtente(LocalDate data) {

        int idade = (int) ChronoUnit.YEARS.between(data, LocalDate.now());

        return idade;
    }
       
    
}
