/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Representa o inicio do programa contém a main
 *
 * @author pcoelho/rmarques
 */
public class Apooruipaulo {


    /**
    * 
     *Objecto scanner de leitura de input de dados do teclado do utilizador
     * @author pcoelho/rmarques
     */
    public static Scanner sc = new Scanner(System.in).useDelimiter("\n");
    // flag que verifica se houve alteraçoes no array das marcaçoes
    private static boolean guardar;
    
     /**
    * 
     *Objecto scanner de leitura de input de dados do teclado do utilizador
     * @author pcoelho/rmarques
     * @param args Argumento de método main
     * @throws IOException sem ficheiro
     */
    public static void main(String[] args) throws IOException {

        // Array das marcações
        ArrayList<Marcacao> marcacoes = new ArrayList<Marcacao>(); // array list das marcações
        
        //escolha do menu
        int escolha = 0;

        // define a zona
        String zona = criarZonaMarcacao(marcacoes);

        boolean mostarPrimaButao = false; //boleano para mostrar o caracter de saida apos comandos

        do {
            menu();

            escolha = checkGetInteiro("Escolha uma opção:"); //função checkGetInteiro certifica que o utilizador insere um numero inteiro

            switch (escolha) {
                case 1:

                    //Opção  1: Ler marcação.
                    criarMarcacao(marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 2:

                    //Opção  2: Apagar marcação de utente.
                    escolha = checkGetInteiro("Qual o numero de Utente?");
                    apagarMarcacaoUtente(escolha, marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 3:

                    //Opção 3: Pesquisa de marcação pelo utente.
                    escolha = checkGetInteiro("Qual o numero de Utente?");
                    pesquisarMarcacaoUtente(escolha, marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 4:

                    //Opção  4: Alterar marcação de utente (Local, data, hora ou Enfermeiro)
                    escolha = checkGetInteiro("Qual o numero de Utente?");
                    alterarMarcacaoUtente(escolha, marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 5:

                    //Opção  5: Pesquisar de marcações num intervalo de idades
                    pesquisarMarcacaoIntervaloIdade(marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 6:

                    //Opção  6: Pesquisar de marcações coma uma marca e/ou lote de vacina
                    pesquisarMarcacaoVacina(marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 7:

                    //Opção  7: Pesquisar indivíduos presentes num determinado dia e local
                    System.out.println("Pessoas Presentes num determinado dia e local: ");
                    pesquisarPessoasLocalDia(marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 8:

                    // Opção  8: Pesquisar marcações de um determinado dia
                    System.out.println("Qual o dia que quer as marcação?");
                    pesquisarMarcacaoDia(lerData(), marcacoes);
                    mostarPrimaButao = true;
                    break;
                case 9:

                    //Opção  9: Pesquisar marcações num determinado local.
                    pesquisarMarcacaoLocal(marcacoes);
                    mostarPrimaButao = true;
                    break;

                case 10:

                    //Opçao 10:Guardar em ficheiro a informacao de uma marcação
                    escreveFicheiroMarcacoes(marcacoes, zona);
                    mostarPrimaButao = true;
                    break;

                case 11:

                    //Opçao 11:Ler ficheiro a informacao de uma marcação
                    lerFicheiroMarcacoes(marcacoes, zona);
                    mostarPrimaButao = true;
                    break;
                case 12:
                    
                    //Opçao 12:Alterar Zona
                    verificarAlteracoes(marcacoes, zona);
                    criarZonaMarcacao(marcacoes);
                    break;

                case 0:
                    verificarAlteracoes(marcacoes, zona);
                    break;

                default:
                    System.out.println("Valor escolhido inválido");
                    break;
            }

            // Impedir que haja algum erro ao ler um int.
            try {
                if (mostarPrimaButao) {
                    mostarPrimaButao = false;
                    System.out.println("> Prima 1 para continuar.");
                    sc.nextInt();
                }
            } catch (Exception ex) {
                sc.next();//"Limpar" o reader
            }

        } while (escolha != 0);

    }

    /**
     * Procedimento para mostrar o menu e suas opções
     */
    static void menu() {

        System.out.println("");
        System.out.println("Opção  1: Ler marcação.");
        System.out.println("Opção  2: Apagar marcação de utente.");
        System.out.println("Opção  3: Pesquisa de marcação pelo utente.");
        System.out.println("Opção  4: Alterar marcação de utente (Local, data, hora ou Enfermeiro).");
        System.out.println("Opção  5: Pesquisar de marcações num intervalo de idades.");
        System.out.println("Opção  6: Pesquisar de marcações de uma marca e/ou lote de vacina.");
        System.out.println("Opção  7: Pesquisar indivíduos presentes num determinado dia e local.");
        System.out.println("Opção  8: Pesquisar marcações de um determinado dia.");
        System.out.println("Opção  9: Pesquisar marcações num determinado local.");
        System.out.println("Opção 10: Guarde em ficheiro a informação de marcações.");
        System.out.println("Opção 11: Ler de um ficheiro a informação de marcações.");
        System.out.println("Opção 12: Mudar ou criar nova zona.");
        System.out.println("");
        System.out.println("Opção  0: Para sair.");

    }

    /**
     * Procedimento para criar/ler uma marcação
     *
     * @param marcacoes //arrayList com todas as marcacoes em memoria
     */
    public static void criarMarcacao(ArrayList<Marcacao> marcacoes) {

        Marcacao novaMarcacao = new Marcacao();
        Local novoCentroSaude = new Local();
        Hospital novoHospital = new Hospital();
        Pavilhao novoPavilhao = new Pavilhao();
        Vacina novaVacina = new Vacina();
        boolean localValido = false;
        int opcao = 0;

        do {
            
            // tipo de local a ser criado
            opcao = checkGetInteiro("Qual o local da consulta? (1-Centro de Saude 2-Hospital 3-Pavilhao)");
            try {

                switch (opcao) {
                    case 1:
                        // ler local centro de saúde
                        System.out.println("Centro de saúde: ");
                        novoCentroSaude = lerCentroSaude();
                        novaMarcacao.setLocalVacinacao(novoCentroSaude);
                        localValido = true;
                        break;

                    case 2:
                        // ler local hospital
                        System.out.println("Hospital: ");
                        novoHospital = lerHospital();
                        novaMarcacao.setLocalVacinacao(novoHospital);
                        localValido = true;
                        break;

                    case 3:
                        // ler pavilhao
                        System.out.println("Pavilhão: ");
                        novoPavilhao = lerPavilhao();
                        novaMarcacao.setLocalVacinacao(novoPavilhao);
                        localValido = true;
                        break;

                    default:
                        break;
                }
            } catch (Exception e) {
                System.out.println("erro menu ");
            }

            // Pode ler as restantes informacoes, se tiver um local válido
            if (localValido) {
                //ler Data de marcação  
                System.out.println("Data de Vacinação:");
                novaMarcacao.setDataMarcacao(lerData());
                //ler Hora de marcação 
                System.out.println("Hora de Vacinação:");
                novaMarcacao.setHoraMarcacao(lerHora());
                // ler enfermeiro
                System.out.println("Ler Enfermeiro:");
                novaMarcacao.setEnfermeiro(lerEnfermeiro());
                //ler utente
                System.out.println("Ler Utente:");
                novaMarcacao.setUtente(lerUtente());
                //Ler vacina
                System.out.println("Ler Vacina:");
                novaMarcacao.setVacina(lerVacina());

                //adicionar nova marcação para o arraylist
                marcacoes.add(novaMarcacao);

                //mostar a marcacao
                Marcacao.mostrarMarcacao(novaMarcacao);
            }

        } while (opcao != 1 && opcao != 2 && opcao != 3);

        //Foram adicionadas marcações ao array marcações
        guardar = true;

    }

    /**
     * Função Ler informação de um centro de saúde
     *
     * @return novoCentro de Saude retorna um Local do tipo centro de saude
     */
    public static Local lerCentroSaude() {
        Local novoCentroSaude = new Local();

        System.out.println("Qual o nome do Local");
        novoCentroSaude.setNomeLocal(sc.next());
        System.out.println("Qual a morada?");
        novoCentroSaude.setMorada(sc.next());
        novoCentroSaude.setTelefone(checkGetInteiro("Qual o telefone?"));

        return novoCentroSaude;
    }

    /**
     * Função ler informacao de um hospital
     *
     * @return novoHospital retorna um Local do tipo hospital
     */
    public static Hospital lerHospital() {
        Hospital novoHospital = new Hospital();

        System.out.println("Qual o nome do Local");
        novoHospital.setNomeLocal(sc.next());
        System.out.println("Qual a morada?");
        novoHospital.setMorada(sc.next());
        novoHospital.setTelefone(checkGetInteiro("Qual o telefone?"));
        System.out.println("Qual o nome do Edificio?");
        novoHospital.setNomeEdif(sc.next());
        novoHospital.setExtenTelef(checkGetInteiro("Qual a extensao Telefonica?"));

        return novoHospital;

    }

    /**
     * Função ler informacao de um Pavilhao
     *
     * @return novoPavilhao retorna um Local do tipo Pavilhao
     */
    public static Pavilhao lerPavilhao() {
        Pavilhao novoPavilhao = new Pavilhao();

        System.out.println("Qual o nome do Local");
        novoPavilhao.setNomeLocal(sc.next());
        System.out.println("Qual a morada?");
        novoPavilhao.setMorada(sc.next());
        novoPavilhao.setTelefone(checkGetInteiro("Qual o telefone?"));
        novoPavilhao.setNumSecPavilhao(checkGetInteiro("Qual o numero de secção?"));

        return novoPavilhao;

    }

    /**
     * Função ler uma data de marcação formato AAAA-MM-DD
     *
     * @return data retorna data inserida
     */
    static LocalDate lerData() {
        String dataString = "";
        LocalDate data;

        do {
            try {
                System.out.println("Introduza uma data com formato AAAA-MM-DD");
                dataString = sc.next();
                data = LocalDate.parse(dataString);
                break;
            } catch (Exception e) {
                System.out.println("Data com formato errado");
            }
        } while (true);

        return data;
    }

    /**
     * Função ler uma Hora de marcação formato HH:MM
     *
     * @return hora retorna hora inserida
     */
    public static LocalTime lerHora() {
        LocalTime hora = LocalTime.now();
        String horaString = "";
        do {
            try {
                System.out.println("Introduza uma hora com formato HH:MM");
                horaString = sc.next();
                hora = LocalTime.parse(horaString);
                break;
            } catch (Exception e) {
                System.out.println("Hora com formato errado");
            }
        } while (true);

        return hora;
    }

    /**
     * Função ler informacao do Enfermeiro
     *
     * @return novoEnfermeiro retorna enfermeiro criado
     */
    public static Enfermeiro lerEnfermeiro() {
        Enfermeiro novoEnfermeiro = new Enfermeiro();

        System.out.println("Qual o Nome?");
        novoEnfermeiro.setNomePessoa(sc.next());
        novoEnfermeiro.setNumTelm(checkGetInteiro("Qual o numero de telemovel?"));
        novoEnfermeiro.setNumCedProf(checkGetInteiro("Qual o numero da cedula?"));

        return novoEnfermeiro;

    }

    /**
     * Função ler informacao do Utente
     *
     * @return novoUtente retorna utente criado
     */
    static Utente lerUtente() {
        Utente novoUtente = new Utente();
        String sexo = null;

        System.out.println("Qual o Nome?");
        novoUtente.setNomePessoa(sc.next());
        novoUtente.setNumTelm(checkGetInteiro("Qual o numero de telemovel?"));
        System.out.println("Qual o Sexo? M(Masculino) F(Feminino) O(Outro)");
        do {

            sexo = sc.next().toUpperCase();

            if (!"M".equals(sexo) && !"F".equals(sexo) && !"O".equals(sexo)) {
                System.out.println("Valor Inválido por favor escolha entre: M(Masculino) F(Feminino) O(Outro)");
            }
        } while (!"M".equals(sexo) && !"F".equals(sexo) && !"O".equals(sexo));
        novoUtente.setsexo(sexo.charAt(0));
        System.out.println(novoUtente.getSexo());
        System.out.println("Data de nascimento"); //Funcao Data testar se o nascimento é antes de hoje
        novoUtente.setDataNascimento(lerData());
        novoUtente.setNumeroUtente(checkGetInteiro("Qual o numero de Utente?"));

        return novoUtente;
    }

    /**
     * Função ler Informacao da Vacina
     *
     * @return novoVacina retorna vacina criada
     */
    static Vacina lerVacina() {
        Vacina novoVacina = new Vacina();
        int escolha = 0;

        do {
            escolha = checkGetInteiro("Qual a marca da Vacina, (1) Pfizer/BioNTech, (2) Moderna, (3) Oxford/AstraZeneca?");

            switch (escolha) {
                case 1:

                    //Opção 1: Pfizer/BioNTech
                    novoVacina.setMarca("Pfizer/BioNTech");
                    break;

                case 2:

                    //Opção 2: Moderna
                    novoVacina.setMarca("Moderna");
                    break;

                case 3:
                    //Opção 3:Oxford/AstraZeneca
                    novoVacina.setMarca("Oxford/AstraZeneca");
                    break;

                default:
                    System.out.println("Escolha uma das 3 opções");
                    sc.next(); //limpar reader

            }

        } while (escolha != 1 && escolha != 2 && escolha != 3);

        novoVacina.setLote(checkGetInteiro("Qual o lote da Vacina"));

        return novoVacina;
    }

    /**
     * Procedimento elimina no arraylist a marcacao de um utente pelo número de
     * utente indicado
     *
     * @param numUtente numero de utente a apagar
     * @param marcacoes array com as marcaçoes
     */
    public static void apagarMarcacaoUtente(int numUtente, ArrayList<Marcacao> marcacoes) {
        boolean existe = false;

        existe = marcacoes.removeIf(marcacao -> marcacao.getUtente().getnUtente() == numUtente); // metodo remmoveIf permite apagar quando se intera as marcações
        if (existe) {
            System.out.println("Marcaçôes do Utente " + numUtente + " removidas");
            //Foi apagada uma marcação no array das marcações
            guardar = true;
        } else {
            System.out.println("Nenhuma marcação com este numero!");
        }
    }

    /**
     * Procedimento pesquisa e mostra se existe uma marcação pelo número de
     * utente
     *
     * @param numUtente numero de utente a pesquisar
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoUtente(int numUtente, ArrayList<Marcacao> marcacoes) {
        boolean existe = false;

        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getUtente().getnUtente() == numUtente) { //Verifica se o numero de utente da marcacao atual corresponde ao numero introduzido por parametro
                Marcacao.mostrarMarcacao(marcacao);
                existe = true;
            }
        }
        if (!existe) {
            System.out.println("Não existe marcacao com esse numero");
        }
    }

    /**
     * Procedimento da marcacao com numero de utente escolhido: modifica o
     * Local, data, hora e enfermeiro mediante escolha do utilizador
     *
     * @param numero (numero de utente)
     * @param marcacoes (lista de marcacoes)
     */
    public static void alterarMarcacaoUtente(int numero, ArrayList<Marcacao> marcacoes) {
        String opcao = "s";
        boolean existe = false;

        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getUtente().getnUtente() == numero) { //Verifica se o numero de utente da marcacao atual corresponde ao numero introduzido por parametro
                existe = true;

                System.out.println("Deseja alterar o local ? (s-Sim)");
                opcao = sc.next().toLowerCase(); //lowercase para o caso de o utilizador escrever S
                if (opcao.charAt(0) == 's') {
                    alterarLocal(marcacao);
                    guardar = true;
                } else {
                    System.out.println("Local não alterado");
                }

                System.out.println("Deseja alterar a Data ? (s-Sim)");
                opcao = sc.next().toLowerCase();
                if (opcao.charAt(0) == 's') {
                    alterarDataMarcacao(marcacao);
                    guardar = true;
                } else {
                    System.out.println("Data não alterada");
                }

                System.out.println("Deseja alterar a Hora ? (s-Sim)");
                opcao = sc.next().toLowerCase();
                if (opcao.charAt(0) == 's') {
                    alterarHoraMarcacao(marcacao);
                    guardar = true;
                } else {
                    System.out.println("Hora não alterada");
                }

                System.out.println("Deseja alterar o Enfermeiro ? (s-Sim)");
                opcao = sc.next().toLowerCase();
                if (opcao.charAt(0) == 's') {
                    marcacao.setEnfermeiro(lerEnfermeiro());
                    guardar = true;
                } else {
                    System.out.println("Enfermeiro não alterado");
                }
            }
        }
        if (!existe) {
            System.out.println("Não existe marcações para este utente");
        }

    }

    /**
     * Procedimento para Alterar o Local da marcacao
     *
     * @param marcacao array com as marcaçoes
     */
    public static void alterarLocal(Marcacao marcacao) {
        System.out.println("Qual o novo local ? 1-Centro 2-Hospital 3-Pavilhao");
        int escolha = sc.nextInt();

        switch (escolha) {
            case 1:
                marcacao.setLocalVacinacao(lerCentroSaude());
                break;
            case 2:
                marcacao.setLocalVacinacao(lerHospital()); //estas funcoes nao precisam de parametros por isso nao criamos novos locais
                break;
            case 3:
                marcacao.setLocalVacinacao(lerPavilhao()); //estas funcoes nao precisam de parametros por isso nao criamos novos locais
                break;
            default:
                System.out.println("Escolha invalida");
                break;
        }
    }

    /**
     * Procedimento Menu para selecciona qual os parametros da Vacina queremos
     * pesquisar nas marcacoes
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoVacina(ArrayList<Marcacao> marcacoes) {
        int escolha = 0;

        escolha = checkGetInteiro("Deseja procurar por (1)Marca (2)Lote ou (3)Ambos");

        switch (escolha) {
            case 1:
                pesquisarMarcacaoVacinaMarca(marcacoes);
                break;
            case 2:
                pesquisarMarcacaoVacinaLote(marcacoes);
                break;
            case 3:
                pesquisarMarcacaoVacinaMarcaLote(marcacoes);
                break;
            default:
                System.out.println("Escolha Invalida");
                break;

        }
    }

    /**
     * Procedimento pesquisa as marcações pelo marca da vacina
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoVacinaMarca(ArrayList<Marcacao> marcacoes) {
        String marca;
        boolean existe = false; //Vai servir para enviar mensagem caso nao exista marcacao com marca inserida

        System.out.println("Insira a marca a procura: ");
        marca = sc.next();
        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getVacina().getMarca().toLowerCase().equals(marca.toLowerCase())) { // uma vez que estamos a comparar Strings usamos o .equals em vez de ==
                Marcacao.mostrarMarcacao(marcacao);
                existe = true;
            }
        }

        if (!existe) {
            System.out.println("Não existem marcacoes com essa vacina");

        }
    }

    /**
     * Procedimento pesquisa as marcações pelo lote da vacina
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoVacinaLote(ArrayList<Marcacao> marcacoes) {
        int lote;
        boolean existe = false;

        lote = checkGetInteiro("Insira o lote a procura: ");

        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getVacina().getLote() == lote) {
                Marcacao.mostrarMarcacao(marcacao);
                existe = true;
            }
        }

        if (!existe) {
            System.out.println("Não existem marcacoes com essa vacina");

        }
    }

    /**
     * Procedimento pesquisa as marcações pelo lote e/ou marca da vacina
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoVacinaMarcaLote(ArrayList<Marcacao> marcacoes) {
        int lote, escolha;
        String marca;
        boolean existe = false;

        try {
            System.out.println("Insira a Marca a procura: ");
            marca = sc.next();
            lote = checkGetInteiro("Insira o lote a procura: ");

            for (Marcacao marcacao : marcacoes) {
                if (marcacao.getVacina().getLote() == lote && marcacao.getVacina().getMarca().toLowerCase().equals(marca.toLowerCase())) { // procura por lote E marca
                    Marcacao.mostrarMarcacao(marcacao);
                    existe = true;
                }
            }

            if (!existe) {
                System.out.println("Não existem marcacoes com essa vacina");

            }
        } catch (Exception e) {
            System.out.println("Inseriu opção Invalida");
            sc.next(); //"Limpar" o reader

        }
    }

    /**
     * Prodedimento actualiza a Data de uma marcacao
     *
     * @param marcacao array com as marcaçoes
     */
    public static void alterarDataMarcacao(Marcacao marcacao) {

        System.out.println("A Data de marcação actual: " + marcacao.getDataMarcacao().format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));
        System.out.println("Qual é a nova da data de marcação:");
        //actualizar a data
        marcacao.setDataMarcacao(lerData());
        System.out.println("Nova data registada: " + marcacao.getDataMarcacao().format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));

    }

    /**
     * Procedimento actualiza a Hora de marcacao
     *
     * @param marcacao array com as marcaçoes
     */
    public static void alterarHoraMarcacao(Marcacao marcacao) {

        System.out.println("A Hora de marcação actual: " + marcacao.getHoraMarcacao().truncatedTo(ChronoUnit.MINUTES));
        System.out.println("Qual é a nova da Hora de marcação:");
        //actualizar a hora
        marcacao.setHoraMarcacao(lerHora());
        System.out.println("Nova hora registada: " + marcacao.getHoraMarcacao().truncatedTo(ChronoUnit.MINUTES));

    }

    /**
     * Procedimento Mostra se existirem as marcações de um determinado dia
     *
     * @param data data a pesquisar
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoDia(LocalDate data, ArrayList<Marcacao> marcacoes) {
        boolean existe = false;

        for (Marcacao marcacao : marcacoes) {
            if (marcacao.getDataMarcacao().equals(data)) {
                Marcacao.mostrarMarcacao(marcacao);
                existe = true;
            }
        }

        if (!existe) {
            System.out.println("Não existe marcações neste dia.");
        }

    }

    /**
     * Procedimento Mostra se existirem as marcações de um determinado intervalo
     * de idade
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoIntervaloIdade(ArrayList<Marcacao> marcacoes) {
        boolean existe = false;
        int[] intIdade = new int[2]; // array que vai conter o intervalo
        int cont = 0; // contador de posição de intervalo

        System.out.println("Marcações num intervalo de idades");

        do {
            if (cont == 0) {
                intIdade[0] = checkGetInteiro("Introduza a idade inicial do intervalo: ");
                cont += 1;
            } else if (cont == 1) {
                intIdade[1] = checkGetInteiro("Introduza a idade final do intervalo: ");
                cont += 1;
            }
        } while (cont != 2);

        //ordenar array do intervalo de idade
        Arrays.sort(intIdade);

        // mostrar marcacoes
        try {
            for (Marcacao marcacao : marcacoes) {
                if (Utente.idadeUtente(marcacao.getUtente().getDataNascimento()) >= intIdade[0] && Utente.idadeUtente(marcacao.getUtente().getDataNascimento()) <= intIdade[1]) {
                    Marcacao.mostrarMarcacao(marcacao);
                    existe = true;
                }
            }
            if (!existe) {
                System.out.println("Não existe marcações neste intervalo de idade.");
            }
        } catch (Exception e) {
            sc.next(); //"Limpar" o reader   
        }

    }

    /**
     * Procedimento Mostra o enfermeiro/utente num determinado dia num
     * determinado local
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarPessoasLocalDia(ArrayList<Marcacao> marcacoes) {
        String nomeLocal;
        LocalDate data;
        boolean existe = false;
        ArrayList<Integer> enfermeiroPresente = new ArrayList<>(); // array dos numero de cédula profissional dos enfermeiros (evita a duplicação da escrita)
        ArrayList<Integer> utentePresente = new ArrayList<>(); // array dos número de utente dos utentes (evita a duplicação da escrita)

        data = lerData();

        try {
            System.out.println("Insira o nome do Local: ");
            nomeLocal = sc.next();

            for (Marcacao marcacao : marcacoes) {

                if (marcacao.getLocalVacinacao().getNome().toLowerCase().equals(nomeLocal.toLowerCase()) && marcacao.getDataMarcacao().equals(data)) { // procura por Local pelo nome e data
                    //
                    if (!enfermeiroPresente.contains(marcacao.getEnfermeiro().getNumCedProf())) {

                        enfermeiroPresente.add(marcacao.getEnfermeiro().getNumCedProf());
                        System.out.println("Nome: " + marcacao.getEnfermeiro().getNome() + " Telefone: " + marcacao.getEnfermeiro().getNumTelem() + " Categoria: Enfermeiro");

                    }
                    if (!utentePresente.contains(marcacao.getUtente().getnUtente())) {
                        
                        utentePresente.add(marcacao.getUtente().getnUtente());
                        System.out.println("Nome: " + marcacao.getUtente().getNome() + " Telefone: " + marcacao.getUtente().getNumTelem() + " Categoria: Utente");

                    }
                    existe = true;
                }
            }
            if (!existe) {
                System.out.println("Não marcações neste dia");
            }

        } catch (Exception e) {
            sc.next(); //"Limpar" o reader

        }

    }

    /**
     * Procedimento pesquisa as marcações pelo nome do local
     *
     * @param marcacoes array com as marcaçoes
     */
    public static void pesquisarMarcacaoLocal(ArrayList<Marcacao> marcacoes) {
        String nomeLocal;
        boolean existe = false;

        try {
            System.out.println("Insira o nome do Local: ");
            nomeLocal = sc.next();

            for (Marcacao marcacao : marcacoes) {
                if (marcacao.getLocalVacinacao().getNome().toLowerCase().equals(nomeLocal.toLowerCase())) { // procura por Local pelo nome
                    Marcacao.mostrarMarcacao(marcacao);
                    existe = true;
                }
            }
            if (!existe) {
                System.out.println("Não existem marcacoes neste local");
            }

        } catch (Exception e) {
            sc.next(); //"Limpar" o reader

        }
    }

    /**
     * Procedimento para escrever num ficheiro de texto o arraylist das
     * marcacoes
     *
     * @param marcacoes array com as marcaçoes
     * @param zona zona 
     * @throws IOException excepção de ficheiro
     */
    public static void escreveFicheiroMarcacoes(ArrayList<Marcacao> marcacoes, String zona) throws IOException {
        BufferedWriter wr = null;
        String sep = "|";
//        File temp = new File("files\\" + zona +".txt");
//        temp.delete();
        try {
            FileWriter ficheiroSaida = new FileWriter("files\\" + zona + ".txt");

            wr = new BufferedWriter(ficheiroSaida);

            for (Marcacao marcacao : marcacoes) {

                // Tipo de local
                if ((marcacao.getLocalVacinacao() instanceof Hospital)) {
                    wr.write("H" + sep);
                } else if (marcacao.getLocalVacinacao() instanceof Pavilhao) {
                    wr.write("P" + sep);
                } else {
                    wr.write("C" + sep);
                }
                // Local 
                wr.write(marcacao.getLocalVacinacao().getNome() + sep + marcacao.getLocalVacinacao().getMorada() + sep + marcacao.getLocalVacinacao().getTelefone() + sep);

                // Se o local for hospital
                if (marcacao.getLocalVacinacao() instanceof Hospital) {
                    wr.write(((Hospital) marcacao.getLocalVacinacao()).getNomeEdif() + sep + ((Hospital) marcacao.getLocalVacinacao()).getExtenTelef() + sep);
                }
                // Se o local  for um pavilhão
                if (marcacao.getLocalVacinacao() instanceof Pavilhao) {
                    wr.write(((Pavilhao) marcacao.getLocalVacinacao()).getNumSecPavilhao() + sep);
                }
                // Data de marcacao
                wr.write(marcacao.getDataMarcacao() + sep);
                // hora de marcação
                wr.write(marcacao.getHoraMarcacao().truncatedTo(ChronoUnit.MINUTES) + sep);
                // Enfermeiro
                wr.write(marcacao.getEnfermeiro().getNome() + sep + marcacao.getEnfermeiro().getNumTelem() + sep
                        + marcacao.getEnfermeiro().getNumCedProf() + sep);

                // Utente
                wr.write(marcacao.getUtente().getNome() + sep + marcacao.getUtente().getNumTelem() + sep + marcacao.getUtente().getSexo() + sep
                        + marcacao.getUtente().getDataNascimento() + sep + marcacao.getUtente().getnUtente() + sep);

                // Vacina
                wr.write(marcacao.getVacina().getMarca() + sep + marcacao.getVacina().getLote());

                wr.write("\n");
            }
            guardar = false;
        } catch (IOException ex) {
            System.out.println("Erro a escrever ficheiro das marcações");
        } finally {
            wr.close();

        }
    }

    /**
     * Procedimento para ler um ficheiro de texto as marcações e passar para o
     * arraylist das marcacoes
     *
     * @param marcacoes array com as marcaçoes
     * @param zona string com o nome da zona
     * @throws IOException excepção de ficheiro
     */
    public static void lerFicheiroMarcacoes(ArrayList<Marcacao> marcacoes, String zona) throws IOException {
        BufferedReader read = null;
        
        // testa se o array foi alterado para não escerver por cima e apagar as alterações não gravadas
        if (guardar == true) {

            System.out.println("Houve alterações guarde anntes de ler o ficheiro");
        } else {
            try {
                FileReader ficheiroEntrada = new FileReader("files\\" + zona + ".txt");
                read = new BufferedReader(ficheiroEntrada);
                String linha = null;
                marcacoes.clear();

                while ((linha = read.readLine()) != null) {

                    String[] linhaPartes = linha.split("\\|");
                    marcacoes.add(criarMarcacao(linhaPartes));

                }
                read.close();
            } catch (FileNotFoundException o) {
                System.out.println("Grave o ficheiro antes de consultar");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Procedimento para ler uma linha do ficheiro de texto e converter em
     * objecto Marcacao
     *
     * @param linha array com os dados da marcaçao
     * @return novaMarcacao retorna a nova marcaçao
     */
    public static Marcacao criarMarcacao(String[] linha) {
        Marcacao novaMarcacao;
        Local novoLocal;
        LocalDate novaData;
        LocalTime novaHora;
        Enfermeiro novoEnfermeiro;
        Utente novoUtente;
        Vacina novaVacina;

        // O primeiro caracter de cada linha do ficheiro representa o tipo de local
        if ("H".equals(linha[0])) {  // H = Hospital
            novoLocal = new Hospital(linha[1], linha[2],
                    Integer.parseInt(linha[3]), linha[4], Integer.parseInt(linha[5])); // Cria novo Hospital e envia para o construtor as posicoes no vecto Linha respetivas
            novaData = criarData(linha[6]);
            novaHora = criarHora(linha[7]);
            novoEnfermeiro = criarEnfermeiro(linha, 8);
            novoUtente = criarUtente(linha, 11);
            novaVacina = criarVacina(linha, 16);
        } else if ("P".equals(linha[0])) { // P = Pavilhao
            novoLocal = new Pavilhao(linha[1], linha[2],
                    Integer.parseInt(linha[3]), Integer.parseInt(linha[4]));
            novaData = criarData(linha[5]);
            novaHora = criarHora(linha[6]);
            novoEnfermeiro = criarEnfermeiro(linha, 7);
            novoUtente = criarUtente(linha, 10);
            novaVacina = criarVacina(linha, 15);
        } else { // Centro de Saude
            novoLocal = new Local(linha[1], linha[2],
                    Integer.parseInt(linha[3]));
            novaData = criarData(linha[4]);
            novaHora = criarHora(linha[5]);
            novoEnfermeiro = criarEnfermeiro(linha, 6);
            novoUtente = criarUtente(linha, 9);
            novaVacina = criarVacina(linha, 14);
        }
        novaMarcacao = new Marcacao(novoLocal, novaData, novaHora, novoEnfermeiro, novoUtente, novaVacina);
        return novaMarcacao;
    }

    /**
     * Procedimento para Criar um Objecto LocalDate a partir do ficheiro
     *
     * @param data recebe a data como String
     * @return novaData retorna a data como LocalDate
     */
    public static LocalDate criarData(String data) {

        LocalDate novaData = LocalDate.parse(data);

        return novaData;
    }

    /**
     * Procedimento para Criar um Objecto LocalTime a partir do ficheiro
     *
     * @param hora recebe a hora como String
     * @return novaHora retorna a hora como LocalTime
     */
    public static LocalTime criarHora(String hora) {

        LocalTime novaHora = LocalTime.parse(hora);

        return novaHora;
    }

    /**
     * Procedimento para Criar um Objecto LocalTime a partir do ficheiro
     *
     * @param linha array com os valores
     * @param i posicao do valor no array linha
     * @return novoEnfermeiro retorna novo Enfermeiro
     */
    public static Enfermeiro criarEnfermeiro(String[] linha, int i) {
        Enfermeiro novoEnfermeiro;
        novoEnfermeiro = new Enfermeiro(linha[i], Integer.parseInt(linha[i + 1]), Integer.parseInt(linha[i + 2]));

        return novoEnfermeiro;

    }

    /**
     * Procedimento para Criar um Objecto Utente a partir do ficheiro
     *
     * @param linha array com os valores
     * @param i posicao do valor no array linha
     * @return novoUtente retorna Utente criado
     */
    public static Utente criarUtente(String[] linha, int i) {
        Utente novoUtente;
        novoUtente = new Utente(linha[i], Integer.parseInt(linha[i + 1]), linha[i + 2].charAt(0),
                LocalDate.parse(linha[i + 3]), Integer.parseInt(linha[i + 4]));

        return novoUtente;
    }

    /**
     * Procedimento para Criar um Objecto Utente a partir do ficheiro
     *
     * @param linha array com os valores
     * @param i posicao do valor no array linha
     * @return novoVacina vacina criada
     */
    public static Vacina criarVacina(String[] linha, int i) {
        Vacina novaVacina;
        novaVacina = new Vacina(linha[i], Integer.parseInt(linha[i + 1]));

        return novaVacina;
    }

    /**
     * Função de verificação se o numero introduzido é inteiro
     *
     * @param question recebe como parametro uma String que vai ser uma questao para um input de um Int
     * @return inteiro retorna o inteiro
     */
    public static int checkGetInteiro(String question) {
        int inteiro = 0;
        boolean varInteiro = false;

        do {

            try {
                System.out.println(question);
                inteiro = sc.nextInt();
                varInteiro = true;
            } catch (InputMismatchException exception) { // se nao for colocado um int fazemos catch do erro

                System.out.println("Valor inválido");
                sc.next();//"Limpar" o reader

                varInteiro = false;
            }
        } while (!varInteiro); // loop enquanto nao for introduzido um int

        return inteiro;
    }
    /**
     * Pede ao utilizador uma zona que pode ser uma de uma lista ja existente de
     * ficheiros ou uma nova, retorna essa zona como string
     *
     * @param marcacoes arrayList com as marcaçoes
     * @return zona escolhida pelo utilizador que corresponde a um ficheiro de
     * texto com o mesmo nome
     */
    public static String criarZonaMarcacao(ArrayList<Marcacao> marcacoes) {

        File files = null;
        File[] zonas;
        int escolha = 0;
        String novaZona;
        guardar = false; // ao iniciar nova zona a flag das alteracoes volta a false

        do {

            // variavel da pasta files
            files = new File("files");

            // Array do conteudo da pasta files
            zonas = files.listFiles();

            System.out.println("Zonas já existentes:");
            // mostra as zonas
            for (int i = 0; i < zonas.length; i++) {
                // replaceAll vai retirar do nome impresso o .txt (ex. em vez de porto.txt vai aparecer apenas porto
                System.out.println((i + 1) + " " + zonas[i].getName().replaceAll("\\.txt", ""));
            }

            //pergunta zona
            escolha = checkGetInteiro("escolha o número da zona ou 0 para criar zona nova");

            // segundo as zonas 
            if ((escolha > 0) && (escolha < (zonas.length + 1))) {

                System.out.println("Escolha: " + zonas[escolha - 1].getName().replaceAll("\\.txt", ""));

                try {
                    lerFicheiroMarcacoes(marcacoes, zonas[escolha - 1].getName().replaceAll("\\.txt", "")); //passamos para memoria toda a informacao em ficheiro

                } catch (IOException ex) {
                    Logger.getLogger(Apooruipaulo.class.getName()).log(Level.SEVERE, null, ex);
                }

                return zonas[escolha - 1].getName().replaceAll("\\.txt", ""); //retorna a posicao escolhida referente a zona
            }

            // criar nova zona
            if (escolha == 0) {

                System.out.println("Escreva o nome da nova zona");
                novaZona = sc.next().toLowerCase(); // o utilizador pode colocar PORTO ou Porto em vez de porto

                boolean check = new File("files", novaZona + ".txt").exists(); // verifica se ja existe ficheiro para a zona escolhida
                if (!check) {
                    return novaZona;

                } else {

                    escolha = checkGetInteiro("Ficheiro já existe deseja voltar ao inicio (1) ou apagar existente e criar novo (2):");

                    switch (escolha) {
                        case 1:

                            break;

                        case 2:
                            return novaZona; //retorna nova zona criada

                        default:
                            break;
                    }

                }

            }

        } while (true);
    }
    /**
     * Verifica a flag guardar, se true avisa o utilizador se quer gravar as alteraçoes feitas no programa
     * @param marcacoes arrayList de marcaçoes
     * @param zona string com o nome da zona em que estamos a trabalhar
     * @throws IOException excepção de ficheiro
     */
    public static void verificarAlteracoes(ArrayList marcacoes, String zona) throws IOException {
        int escolha = 0;
        if (guardar == true) {
            while (escolha != 1 || escolha != 2) {
                escolha = checkGetInteiro("ATENÇÃO existem alterações por guardar! (1)sair (2)guardar");
                switch (escolha) {
                    case 1:
                        return;

                    case 2:
                        System.out.println("A gravar ficheiro");
                        System.out.println(".............");
                        escreveFicheiroMarcacoes(marcacoes, zona); // grava alteraçoes
                        return;

                    default:
                        System.out.println("Opão invalida");
                        break;
                }
            }
        } else {
            escreveFicheiroMarcacoes(marcacoes, zona); // se nao houverem alteraçoes podemos regravar o ficheiro
        }
    }

}
