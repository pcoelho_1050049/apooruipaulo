/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa uma Vacina
 *
 * @author pcoelho
 */
public class Vacina {
    
    private String marca; // Marca da vacina  (Pfizer/BioNTech, Moderna, Oxford/AstraZeneca) 
    private int lote; // lote da vacina
    
    //contrutor
    /**
     * Cria uma Vacina com marca e lote
     *
     * @author pcoelho
     * @param marca marca da Vacina
     * @param lote lote da Vacina
     */
    public Vacina(String marca, int lote){
        this.marca = marca;
        this.lote = lote;
    }
    
    /**
     * Cria uma Vacina sem atributos
     *
     * @author pcoelho
     */
    public Vacina(){
    
    }
    
    //getter Vacina marca
    /**
     * Getter marca de Vacina
     * @author pcoelho
     * @return marca marca da Vacina
     * 
     */
    String getMarca(){
        return this.marca;
    }
    // getter Vacina lote
    /**
     * Getter lote de Vacina
     * @author pcoelho
     * @return lote lote da Vacina
     * 
     */
    int getLote(){
        return this.lote;
    }
    
    // setter marca
    /**
     * Setter marca de Vacina
     * @author pcoelho
     * @param marca marca da Vacina
     * 
     */
    void setMarca(String marca){
        this.marca = marca;
    }
    
// setter Vacina lote
    /**
     * Setter marca de Vacina
     * @author pcoelho
     * @param lote lote da Vacina
     * 
     */
    void setLote(int lote){
        this.lote = lote;
    }
    
    
}
