/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa uma Pessoa
 *
 * @author pcoelho
 */
public class Pessoa {
    
    private String nomePessoa; // nome de Utente ou Enfermeiro
    private int nTelem; // número de telemóvel de Utente ou Enfermeiro 
    
    
    //contrutor
    /**
     * Cria uma Pessoa com nome, nº telemovel
     *
     * @author pcoelho
     * @param nomePessoa nome de Utente ou Enfermeiro
     * @param nTelem nº de telemóvel de Utente ou Enfermeiro
     */
    public Pessoa(String nomePessoa, int nTelem){
        
        this.nomePessoa = nomePessoa;
        this.nTelem = nTelem;
  
    }
    //contrutor
    /**
     * Cria uma Pessoa sem atributos
     *
     * @author pcoelho

     */
    public Pessoa(){
    
    }
    
   // getter numero de cédula profissional
     /**
     * Getter Nome da Pessoa
     * @author pcoelho
     * @return nomePessoa nome de Utente ou Enfermeiro
     * 
     */
    String getNome(){
        return this.nomePessoa;
    }
    
    // getter numero de Telemóvel
     /**
     * Getter Nome da Pessoa
     * @author pcoelho
     * @return nTelem nº de telemóvel de Utente ou Enfermeiro
     * 
     */
    int getNumTelem(){
        return this.nTelem;
    }
    
    // setter numero de Telemóvel
     /**
     * Getter Nome da Pessoa
     * @author pcoelho
     * @param nomePessoa nome de Utente ou Enfermeiro
     * 
     */
    void setNomePessoa(String nomePessoa){
        this.nomePessoa = nomePessoa;
    }
    
    // setter numero de Telemóvel
     /**
     * Getter número de telemóvel
     * @author pcoelho
     * @param nTelem nº de telemóvel de Utente ou Enfermeiro
     * 
     */
    void setNumTelm(int nTelem){
        this.nTelem = nTelem;
    }
    
}
