/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;


/**
 * Representa ua Marcação
 * 
 *
 * @author pcoelho
 */
public class Marcacao {
    
    private Local localVaci; // local de vacinação (centro de saúde, hospital, pavilhao municipal)
    private LocalDate dataMarcacao; // data da marcacao
    private LocalTime horaMarcacao; // hora da marcação
    private Enfermeiro enfermeiro; // Enfermeiro
    private Utente utente; // utente
    private Vacina vacina; // vacina
    
    //Contrutores
    /**
     * Construtor cria uma Marcacao com Local de vacinação, Data, Hora, Enfermeiro e vacina
     *
     * @author pcoelho
     * @param localVaci local de vacinação (centro de saúde, hospital, pavilhao municipal)
     * @param dataMarcacao data da marcacao
     * @param horaMarcacao hora da marcação
     * @param enfermeiro Objecto Enfermeiro
     * @param utente Objecto Utente
     * @param vacina Objecto Vacina
     */
    public Marcacao(Local localVaci,LocalDate dataMarcacao, LocalTime horaMarcacao, Enfermeiro enfermeiro, Utente utente, Vacina vacina){
        this.localVaci = localVaci;
        this.dataMarcacao = dataMarcacao;
        this.horaMarcacao = horaMarcacao;
        this.enfermeiro = enfermeiro;
        this.utente = utente;
        this.vacina = vacina;
    }
    
    //Contrutores
    /**
     * Construtor cria uma Marcacao sem attributos
     *
     * @author pcoelho
     */
    public Marcacao(){
    
    }
  
     //getter Objecto de local de vacinação
     /** Getter Local
     * @author pcoelho
     * @return localVaci local de vacinação (centro de saúde, hospital, pavilhao municipal)
     * 
     */
    Local getLocalVacinacao(){
        return this.localVaci;
    }
    
    // getter data de marcacao
    /** Getter Data de marcação
     * @author pcoelho
     * @return data data da marcacao
     * 
     */
    LocalDate getDataMarcacao(){
        return this.dataMarcacao;
    }
     
    // getter Hora de marcacao
    /** Getter hora de marcação
     * @author pcoelho
     * @return horaMarcacao hora da marcação
     * 
     */
    LocalTime getHoraMarcacao(){
        return this.horaMarcacao;
    }
    
    // getter objecto enfermeiro
    /** Getter objecto enfermeiro
     * @author pcoelho
     * @return enfermeiro Objecto Enfermeiro
     * 
     */
    Enfermeiro getEnfermeiro(){
        return this.enfermeiro;
    }
     
    // getter objecto utente
    /** Getter objecto Utente
     * @author pcoelho
     * @return utente Objecto utente
     * 
     */
    Utente getUtente(){
        return this.utente;
    }
    
    // getter objecto vacina
    /** Getter objecto Vacina
     * @author pcoelho
     * @return vacina objecto Vacina
     * 
     */
    Vacina getVacina(){
        return this.vacina;
    }
    
    
     //setter Objecto de local de vacinação
     /** Getter Local
     * @author pcoelho
     * @param LocalVaci local de vacinação (centro de saúde, hospital, pavilhao municipal)
     * 
     */
    void setLocalVacinacao(Local localVaci ){
        this.localVaci = localVaci ;
    }
    
    // setter data de marcacao
     /** setter Data de marcação
     * @author pcoelho
     * @param dataMarcacao data da marcacao
     */
    void setDataMarcacao(LocalDate dataMarcacao){
        this.dataMarcacao = dataMarcacao;
    }
    // setter Hora de marcacao
    /** Getter hora de marcação
     * @author pcoelho
     * @param horaMarcacao hora da marcação
     * 
     */
    void setHoraMarcacao(LocalTime horaMarcacao){
        this.horaMarcacao = horaMarcacao;
    }
    // setter objecto enfermeiro
    /** Setter objecto enfermeiro
     * @author pcoelho
     * @param enfermeiro Objecto Enfermeiro
     * 
     */
    void setEnfermeiro(Enfermeiro enfermeiro){
        this.enfermeiro = enfermeiro;
    }
    
    // setter objecto utente
    /** setter objecto Utente
     * @author pcoelho
     * @param utente Objecto utente
     * 
     */
    void setUtente(Utente utente){
        this.utente = utente;
    }
     
    // setter objecto vacina
    /** Setter objecto Vacina
     * @author pcoelho
     * @param vacina Objecto Vacina
     * 
     */
    void setVacina(Vacina vacina){
        this.vacina = vacina;
    }
    

    /**
     * Procedimento
     * Mostra para a consola a informação de uma marcacao
     * @param marcacao Objecto Marcacao
     */
    public static void mostrarMarcacao(Marcacao marcacao){
        
        System.out.println("###########################################################################################################");
        System.out.println("Marcação: ");
        System.out.println("Local de Vacinação: " + marcacao.getLocalVacinacao().getNome() );
        System.out.println("Morada : " + marcacao.getLocalVacinacao().getMorada() + " telefone: " + marcacao.getLocalVacinacao().getTelefone() );
        // Se o local for um hospital
        if(marcacao.getLocalVacinacao() instanceof Hospital){
                System.out.println("Nome do Edificio: " + ((Hospital) marcacao.getLocalVacinacao()).getNomeEdif() + " Extensão Telefónica : " + ((Hospital) marcacao.getLocalVacinacao()).getExtenTelef());
        }
        // Se o local  for um pavilhão
        if(marcacao.getLocalVacinacao() instanceof Pavilhao){
            
           System.out.println("Nº de secção do pavilhão: " + ((Pavilhao) marcacao.getLocalVacinacao()).getNumSecPavilhao());
        }
        System.out.println("Data de Marcação: " + marcacao.getDataMarcacao().format(DateTimeFormatter.ofPattern("dd-MM-YYYY")));
        System.out.println("Hora de Marcação: " + marcacao.getHoraMarcacao().truncatedTo(ChronoUnit.MINUTES));
        System.out.println("Enfermeiro: " + marcacao.getEnfermeiro().getNome() + " Cédula profissional: " + marcacao.getEnfermeiro().getNumCedProf() + " nº de telemóvel " 
                           + marcacao.getEnfermeiro().getNumTelem() );
        System.out.println("Utente: " + marcacao.getUtente().getNome() + " sexo: " + marcacao.getUtente().getSexo() + " Data de Nascimento: " 
                           + marcacao.getUtente().getDataNascimento().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))
                           + " Número de utente: " + marcacao.getUtente().getnUtente() + " Número de Telemóvel: " + marcacao.getUtente().getNumTelem() );
        System.out.println("Vacina com marca: " + marcacao.getVacina().getMarca() + " Lote: " + marcacao.getVacina().getLote());
        System.out.println("###########################################################################################################");
        
        
    }        

   }
    

