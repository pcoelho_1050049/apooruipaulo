/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa um Pavilhao
 *
 *
 * @author pcoelho
 */
public class Pavilhao extends Local {

    private int nSeccao; //Número da secção do pavilhão

    //contrutor
    /**
     * Cria um Pavilhao com nome, morada, nome de Edificio e extensão telefónica
     *
     * @author pcoelho
     * @param nome Nome do Pavilhao
     * @param morada Morada do Pavilhao
     * @param telefone nº de telefone do Pavilhao
     * @param nSeccao nº de secção de Pavilhao
     */
    public Pavilhao(String nome, String morada, int telefone, int nSeccao) {
        super(nome, morada, telefone);
        this.nSeccao = nSeccao;
    }
    
     /**
     * Cria um Pavilhaol sem atributos
     *
     * @author pcoelho
     */
    public Pavilhao() {

    }
    
    /**
     * Getter número de secção do Pavilhao
     * @author pcoelho
     * @return nSeccao nº de secção de Pavilhao
     * 
     */
    int getNumSecPavilhao() {
        return this.nSeccao;
    }
    
    /**
     * Setter número de secção do Pavilhao
     * @author pcoelho
     * @param nSeccao nº de secção de Pavilhao
     * 
     */
    void setNumSecPavilhao(int nSeccao) {
        this.nSeccao = nSeccao;
    }



}
