/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa um Hospital
 * 
 *
 * @author pcoelho
 */
public class Hospital extends Local {
    
    private String nomeEdif; //nome do edifício 
    private int exTelef; // extensão telefónica
    
     
    //contrutor
    /**
     * Cria um Hospital com nome, morada, nome de Edificio e extensão telefónica
     *
     * @author pcoelho
     * @param nome nome do Hospital
     * @param morada morada da morada do Hospital
     * @param telefone nº de telefone do Hospital
     * @param nomeEdif nome do Edificio do Hospital
     * @param exTelef nº de extensão de telefone do Hospital
     */
    public Hospital(String nome,String morada, int telefone,String nomeEdif, int exTelef){
        super(nome,morada,telefone);
        this.nomeEdif = nomeEdif;
        this.exTelef = exTelef;
    }
    
    /**
     * Cria um Hospital sem atributos
     *
     * @author pcoelho
     */
    public Hospital(){
    
    }
    

     /**
     * Getter número de Nome de Edificio do Hospital
     * @author pcoelho
     * @return nomeEdif nome do Edificio do Hospital
     * 
     */
    String getNomeEdif(){
        return this.nomeEdif;
    }
    
    // getter numero de cédula profissional
     /**
     * Getter número de Extensão telefónicade do Hospital
     * @author pcoelho
     * @return exTelef  nº de extensão de telefone do Hospital
     * 
     */
    int getExtenTelef(){
        return this.exTelef;
    }
    
     /**
     * Setter Nome do Edificio do Hospital 
     * @author pcoelho
     * @param nomeEdif nome do Edificio do Hospital
     * 
     */
    void setNomeEdif(String nomeEdif){
        this.nomeEdif = nomeEdif;
    }
    
// setter telefone do local
    /**
     * Setter da extensão telefónica do hospital 
     * @author pcoelho
     * @param exTelef  nº de extensão de telefone do Hospital
     * 
     */
    void setExtenTelef(int exTelef){
        this.exTelef = exTelef;
    }
    
    
    
}
