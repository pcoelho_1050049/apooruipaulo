/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa um Enfermeiro
 *
 * @author pcoelho
 */
public class Enfermeiro extends Pessoa {

    private int nCedulaProf; //Número da cédula profissional

    //contrutor
    /**
     * Cria um Enfermeiro com nome, nº telemovel, nº cédula profissional
     *
     * @author pcoelho
     * @param nomePessoa nome do Enfermeiro
     * @param nTelem nº de telemóvel do Enfermeiro
     * @param nCedulaProf nº de de cédula profissional
     */
    public Enfermeiro(String nomePessoa, int nTelem, int nCedulaProf) {
        super(nomePessoa, nTelem);
        this.nCedulaProf = nCedulaProf;
    }
    
     /**
     * Cria um Enfermeiro sem atributos
     *
     * @author pcoelho
     */
    public Enfermeiro() {

    }

    // getter numero de cédula profissional
     /**
     * Getter número da cédula Profissional do Enfermeiro
     * @author pcoelho
     * @return nCedulaProf nº de de cédula profissional
     * 
     */
    int getNumCedProf() {
        return this.nCedulaProf;
    }
    
    // setter numero de cédula profissional
     /**
     * Setter número da cédula Profissional do Enfermeiro
     * @author pcoelho
     * @param nCedulaProf nº de de cédula profissional
     * 
     */
    void setNumCedProf(int nCedulaProf) {
        this.nCedulaProf = nCedulaProf;
    }

}
