/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apooruipaulo;

/**
 * Representa um Local
 * 
 *
 * @author pcoelho
 */
public class Local {
    
    private String nome; // nome do local
    private String morada; //morada do local 
    private int telefone; // telefone do local
    
    
    
    /**
     * Construto cria um Local com nome, morada, nome de Edificio e numero de telefone
     *
     * @author pcoelho
     * @param nome nome do local
     * @param morada morada do local 
     * @param telefone  nº telefone do local
     */
    public Local(String nome,String morada, int telefone){
        this.nome = nome;
        this.morada = morada;
        this.telefone = telefone;
    }
    
     /**
     * Cria um Local sem atributos
     *
     * @author pcoelho
     */
    public Local(){
    
    }
    
    /**
     * Getter nome do Local
     * @author pcoelho
     * @return nome nome do local
     * 
     */
    String getNome(){
        return this.nome;
    }
    
    /**
     * Getter morada do local
     * @author pcoelho
     * @return morada morada do local 
     * 
     */
    String getMorada(){
        return this.morada;
    }
    
    /**
     * Getter numero de telefone do local
     * @author pcoelho
     * @return telefone nº telefone do local
     * 
     */
    int getTelefone(){
        return this.telefone;
    }
    
     /**
     * Setter nome do Local 
     * @author pcoelho
     * @param nome nome do local
     * 
     */
    void setNomeLocal(String nome){
        this.nome = nome;
    }
     
    /**
     * Setter morada do Local 
     * @author pcoelho
     * @param morada morada do local 
     * 
     */
    void setMorada(String morada){
        this.morada = morada;
    }
    /**
     * Setter telefone do Local 
     * @author pcoelho
     * @param telefone nº telefone do local
     * 
     */
    void setTelefone(int telefone){
        this.telefone = telefone;
    }
    
}
